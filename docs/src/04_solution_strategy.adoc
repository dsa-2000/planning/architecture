ifndef::imagesdir[:imagesdir: ../images]

[[section-solution-strategy]]
== Solution Strategy

=== Functional decomposition

Decomposition of DSA-2000 software system components is based on
functional roles the software fulfills within the DSA-2000
project. This decomposition largely follows the subsystem
decomposition of the DSA-2000 project, although there are
exceptions. For example, the RCP subsystem computing hardware hosts
processes in both the radio camera processor and data management
software systems; thus the RCP subsystem does not align perfectly with
the radio camera processor software system.

=== Increase reliability through redundancy

Improve the reliability of software systems by employing
redundancy. Redundancy is enabled by software design, although
implementation of redundancy depends on the software deployment
environment. Strategies for employing redundancy can take any number
of forms, including use of hot or cold spares, sharding of services,
_etc_.

=== Increase reliability and performance through loose coupling

Improve the reliability and performance of the system by employing
loose coupling between software processes. Loose coupling is enabled
by software architecture and design, as well as implementation of
process deployment in an environment. Loose coupling promotes the
reduction in size of failure domains in the system, allowing system
performance to degrade by small increments in many failure scenarios.

=== Open source, third party dependencies

Third party dependencies of all internal DSA-2000 software systems
(_i.e_, all except ARC) should be open source, unless cost and
performance effective alternatives are unavailable.
