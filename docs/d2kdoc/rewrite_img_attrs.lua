--
-- fix up some image attributes
--
-- This filter fixes two issues:
--
-- * asciidoc puts identifiers at the block level, which pandoc+latex
-- see as identifiers for the blocks, not their content. This is a
-- problem when the block is an image block, in which case the
-- identifier is associated with the location of the image macro, not
-- the image itself. These locations may be different because latex
-- often does not place images in relation to the text exactly where
-- the images are included
--
-- * the "diagram-key" class needs to be implemented for pandoc, not
-- just asciidoc html
--
function Div(elem)
  if elem.classes:includes("imageblock", 1) then
    if elem.classes:includes("diagram-key", 1) then
      elem.content = elem.content:walk {
        Image = function(i)
          i.attributes["width"] = "50%"
          return i
        end
      }
      return elem
    end
    if elem.identifier then
      -- lower the identifier to the Image (from the Div)
      elem.content = elem.content:walk {
        Image = function(i)
          i.identifier = elem.identifier
          return i
        end
      }
      elem.identifier = ""
      return elem
    end
  end
end

--
-- convert image links to references
--
-- for subsequent application of pandoc-crossref filter
--
function Link(elem)
  if elem.target:find("#fig:") == 1 then
    return pandoc.Cite(
      "",
      {pandoc.Citation(elem.target:sub(2), "NormalCitation")})
  end
end
