#
# Monitoring and control sub-system (WBS L2: MC)
#

#
# The MC software system comprises a micro service pardigm organized into a Model, View, Controller architecture.
# The microservices are decoupled from all other microservices and only expose their needed monitor data and command set.
# They must handle concurrent commands but no direct monitor requests are made on them.
#
#
mc = softwareSystem "Monitoring and control" "Control flow of commands, configuration  and monitor data" {

  #
  # The MC View comprises open source visualization tools and custom tools for viewing all monitor data, user generated data,
  # logging, Documentation, alarms and Array Health predictions bases on a Machine Learning algorithm
  #
  view = container "MC View" "Visual Presentation of data, monitoring, health of system" {
    grafana = component "Grafana" "Timeseries database viewer. 1..N"
    plotviewer = component "PlotViewer" "Custorm viewer for user generated plots"
    logviewer = component "Kibana" "Log viewer"
    docviewer = component "Web based documentation viewer. Monitor point definitions, Command usage etc"
    alarmview = component "Alarm" "Hierarchial Alarm viewer. What's wrong with system"
    predictionview = component "ML system health prediction display"
  }

  #
  # The MC control manages all process flow between the user/developer and the mc Model. It basically hides the model and provides
  # a more logical API for the user/developer. It also contains the mapping between monitor point data and the DKVS. THis mapping
  # allows the Monitor Data to scale horizontally without any user awareness. This mapping is part of the configuration allowing
  # multiple language specific API to the Monitor data to have the identical mapping. The mapping can change dynamically allowing
  # the horizontal scaling to be seamless. There are caveats to this.
  #
  control = container "MC Control" "Process flow between systems. Hides model implementation" {
    msdkvs = component "Microservice: DKVS access for command and monitor data"
    msdkvs2db = component "Microservice: Transfers DKVS to time-series database"
    mswx = component "Microservice: Provides site weather"
    msauth = component "Microservice: User authentication API"
    msephemeris = component "Microservice: Provides astronomical object ephemeris common for all telescopes"
    ephemeristel = component "API for telescope specific ephemeris"
    msant = component "Microservice: Provides API for MC of antennas."
    masolarpower = component "Microservice: Provides API for solar power"
    msgenericsubsystem = component "Microservice: Provides MC API for subsystems"
    logging = component "Provides API for logging"
  }

  #
  # The MC model stores all state related data vis microservices. It provides API's to this state for the MC Control. At this point, it's
  # still possible to allow the View to use this API for viewing but need to think about atomic updates. I don't like locks so
  # having MC Control manage the View's access to this data is still appealing.
  #
  model = container "MC Model" "Subsystem config, monitor, logging, data products storage and logic" {
    dkvs = component "ETCD" "Distributed Key-value Store. 1..N"
    dkvsdb = component "Influx DB" "dkvs time-series database. 1..N"
    authdb = component "Auth DB" "User authentication database, 1"
    config = component "Config DB" "Array configuration, Antenna locations etc manangement. 1"
    wxdb = component "Weather DB" "Weather database, 1"
    logdb = component "Log DB" "Log database, 1..N"
    plotstor = component "Plot Storage" "User generated plots"
    classifierdb = component "Classifier DB" "Relates time to Array Health Classifiers for ML"
  }


  #
  # Add relationships between MC components.
  #
  control -> model.dkvs "Manages Horizontal Scaling of DKVS"
  control -> model.dkvsdb "Manages Microservice for writing DKVS to Influx"
  control -> model.authdb "Validates Users"
  control -> model.config "Manages Microservice for Array Configuration"
  control -> model.wxdb "Manages Microservice for Weather data"
  control -> model.logdb "Logs all transactions"
  control -> model.plotstor "Provides API for plot storage"
  control -> model.classifierdb "Provides API for Array Health Classification"
  model -> control "Provide API for control"
  control -> model "Encapsulates model API."
  control -> view "Updates view"
  view -> control "Pulls data"
}
