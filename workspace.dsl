#
# DSA-2000 software architecture top-level workspace
#
# The architecture definition is decomposed into a set of files
# included by this file, transitively.
#
workspace "DSA-2000" "Software architecture" {

    # this prevents editing using structurizr.com DSL editor...we want
    # all changes to be made through the git repository
    properties {
        "structurizr.dslEditor" "false"
    }

    # Documentation sub-directory
    !docs docs/src

    # Use hierarchical (scoped) identifiers to reduce the risk of
    # errors.
    !identifiers hierarchical

    # model definition
    !include model.dsl
    # diagrams (views) definition
    !include views.dsl
}
