#
# DSA-2000 software architecture diagram (view) definitions
#
views {
    styles {
        element "filesystem" {
            shape Folder
        }
    }

    systemLandscape "SystemLandscape" "DSA-2000 system landscape" {
        include *
    }

    systemLandscape "Context" "DSA-2000 context" {
        include scientist
        include operator
        include opl
        include mc
        include arc
        include developer
        include engineer
        include operator
        include gitlabService
        include swEngineering
        include s3
        include nanograv
    }

    #
    # RCP subsystem views
    #
    systemContext rcp "RCP-system-context" "RCP subsystem context" {
        include *
    }

    container rcp "RCP-container" "RCP subsystem" {
        include *
        exclude "mc -> dat"
        exclude "dat -> mc"
        exclude "* -> mc"
        exclude rcp.ftd
    }

    component rcp.ipl "IPL-component" "IPL component" {
        include *
        exclude mc
    }

    component rcp.ipl2 "IPL2-component" "IPL2 component" {
        include *
        exclude mc
        exclude dat
    }

    deployment * rcpDeployment "RCP-deployment" "RCP deployment" {
        include *
        exclude rcp.ftd
    }

    #
    # MC subsystem views
    #
    # Note use of MNC in view tags -- this is done for continuity
    # after acronym change. TODO: recreate diagrams with new tags
    # using MC.
    #
    systemContext mc "MNC-system-context" "MC subsystem context" {
        include *
    }

    container mc "MNC-container" "MC subsystem" {
        include *
        exclude rcp
        exclude dat
        exclude opl
        exclude operator
    }

    #
    # DAT subsubtem views
    #
    systemContext dat "DAT-system-context" "DAT subsystem context" {
        include *
    }

    container dat "DAT-container" "DAT subsystem" {
        include *
    }

    dynamic dat "DAT-MP-img" "DAT processing for MP images" {
        rcp.ipl -> rcp.fs "writes a raw MP image"
        rcp.ipl -> dat.taskScheduler "sends notification of new image"
        dat.taskScheduler -> dat.database "updates"
        dat.taskScheduler -> dat.commonInitialImageProc "triggers"
        dat.taskScheduler -> dat.database "updates"
        dat.commonInitialImageProc -> dat.taskScheduler "sends completion notification"
        dat.taskScheduler -> dat.database "updates"
        dat.taskScheduler -> dat.contImageProc "triggers"
        dat.taskScheduler -> dat.database "updates"
        dat.contImageProc -> dat.taskScheduler "sends completion notification"
        dat.taskScheduler -> dat.database "updates"
        dat.taskScheduler -> dat.commonFinalImageProc "triggers"
        dat.taskScheduler -> dat.database "updates"
        dat.commonFinalImageProc -> dat.taskScheduler "sends completion notification"
        dat.taskScheduler -> dat.database "updates"
        dat.taskScheduler -> dat.archiveprepProc "triggers"
        dat.archiveprepProc -> arc "sends data products to"
    }

    #
    # OPL subsubtem views
    #
    systemContext opl "OPL-system-context" "OPL subsystem context" {
        include *
    }

    container opl "OPL-container" "OPL subsystem" {
        include *
    }

    #
    # RCF subsystem views
    #
    systemContext rcf "RCF-system-context" "RCF subsystem context" {
        include *
    }

    #
    # PT subsystem views
    #
    systemContext pt "PT-system-context" "PT subsystem context" {
        include *
    }

    #
    # software engineering services
    #
    container swEngineering "swEngineering-container" "Software engineering supprt" {
        include *
    }

    deployment * localRunnerDeployment "LocalRunner-deployment" "gitlab runner deployment" {
        include *
    }

    styles {
        relationship "Main flow" {
            color red
            dashed false
            thickness 4
        }

        element "Unimplemented" {
        }

        element "Implemented" {
            stroke purple
            strokeWidth 10
        }

        element "Partially implemented" {
            stroke purple
            strokeWidth 10
            border dashed
        }
    }
    theme default
}
