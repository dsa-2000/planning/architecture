#
# Observation planner sub-system (WBS L2: OPL)
#
opl = softwareSystem "Observation planning" {

    hal = container "Automated operations" "System capable of automated array operations"
}
