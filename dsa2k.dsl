#
# DSA-2000 software architecture definitions of
# - people or roles
# - cross sub-system relationships
# - relationships with people or roles
#

#
# relationships
#

# DAT <-> ARC
dat -> arc "loads data products to"

# RCP <-> MC
mc.control.msdkvs -> rcp "gets raw images from"
rcp.ipl -> mc.control.logging "writes logs to"
rcp.ipl2 -> mc.control.logging "writes logs to"
rcp.ftd -> mc.control.logging "writes logs to"
rcp.ipl -> mc.control.msdkvs "writes monitor data to"
rcp.ipl2 -> mc.control.msdkvs "writes monitor data to"
mc.control.msdkvs -> rcp.ipl "provides metadata to"
mc.control.msdkvs -> rcp.ipl.getAntPositions "provides positions to"
mc.control.msdkvs -> rcp.ipl.computeUVW "provides field directions to"
mc.control.msdkvs -> rcp.ipl.preCalFlagging "provides obs metadata to"
mc.control.msdkvs -> rcp.ipl.solveCal "provides obs metadata to"
mc.control.msdkvs -> rcp.ipl.postCalFlagging "provides obs metadata to"
mc.control.msdkvs -> rcp.ipl2 "provides metadata to"
mc.control.msdkvs -> rcp.ipl2.getAntPositions "provides positions to"
mc.control.msdkvs -> rcp.ipl2.computeUVW "provides field directions to"
mc.control.msdkvs -> rcp.ipl2.preCalFlagging "provides obs metadata to"
mc.control.msdkvs -> rcp.ipl2.postCalFlagging "provides obs metadata to"

# RCP <-> RCF
rcp.ipl.exportCal -> rcf "sends cal solutions to"
rcf -> rcp.ipl2.dataCapture "sends voltage data to" "UDP/Ethernet"
rcf -> rcp.ipl.dataCapture "sends voltage data to" "UDP/Ethernet"

# RCP <-> PT
rcf -> pt "sends beamformed voltage data to" "UDP/Ethernet"

# DAT <-> MC
dat -> mc.control.logging "writes logs to"
mc.control.msdkvs -> dat "provides obs metadata to"
dat -> mc.control.msdkvs "writes obs and post-processing metadata to"

# OPL <-> MC
opl.hal -> mc.control "uses"

# DAT <-> RCP
rcp.ipl.exportGrids -> dat.taskScheduler "sends image notifications to" "HTTP"
rcp.ipl2.exportGrids -> dat.taskScheduler "sends image notifications to" "HTTP"
dat -> rcp.fs "reads images from"
dat.commonInitialImageProc -> rcp.fs "reads images from"
dat.contImageProc -> rcp.fs "reads images from"
dat.polImageProc -> rcp.fs "reads images from"
dat.lowresHIImageProc -> rcp.fs  "reads images from"
dat.zoomaImageProc -> rcp.fs "reads images from"
dat.zoombImageProc -> rcp.fs "reads images from"
dat.commonFinalImageProc -> rcp.fs "reads images from"
dat.archiveprepProc -> arc "updates"

# DAT <-> PT
dat.fastpertProc -> pt "gets profiles from"

# MC <-> AST
mc.control -> ast "controls and monitors devices in"
