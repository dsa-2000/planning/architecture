#
# Radio camera processor sub-system (WBS L2: RCP)
#
rcp = softwareSystem "Radio camera processor" "Creates images from radio camera front-end data in real time" {

    fs = container "Output filesystem" "Stores RCP images" {
        tags = "filesystem"
    }

    ipl = container "Imaging pipeline" "Creates images from voltage data, computes calibration solutions" "Legion" {
        getAntPositions = component "Get positions" "Get antenna positions" {
            tags "Unimplemented"
        }
        computeBaselines = component "Compute baselines" "Compute baselines" "Kokkos(CUDA)" {
            tags "Implemented"
        }
        dataCapture = component "Data capture" "Capture and sort voltage data packets" "UDP/Ethernet,libfabric" {
            tags "Implemented"
        }
        crossCorrelate = component "Cross correlation" "Correlate voltage data to produce visibilities" "TCC(CUDA)" {
            tags "Implemented"
        }
        computeWeights = component "Compute weights" "Compute visibility weights" "Kokkos(CUDA)" {
            tags "Implemented"
        }
        normalizeVisibilities = component "Normalize" "Normalize visibilities by weights" "Kokkos(CUDA)" {
            tags "Implemented"
        }
        computeUVW = component "Compute UVW" "Compute UVW coordinates" "Kokkos(CUDA)" {
            tags "Partially implemented"
        }
        preCalFlagging = component "Pre-calibration flagging" "Flag uncalibrated visibilities" {
            tags "Unimplemented"
        }
        solveCal = component "Compute cal solutions" "Compute calibration solutions" {
            tags "Unimplemented"
        }
        applyCal = component "Apply cal solutions" "Apply calibration solutions to visibilities" {
            tags "Unimplemented"
        }
        exportCal = component "Export cal solutions" "Export calibration solutions (for RCF and zoom band instances)" {
            tags "Unimplementd"
        }
        postCalFlagging = component "Post-calibration flagging" "Flag calibrated visibilities" {
            tags "Unimplemented"
        }
        gridVisibilities = component "Gridding" "Grid visibilities" "Kokkos(CUDA)" {
            tags "Implemented"
        }
        exportGrids = component "Export grids" "Export image grids" {
            tags "Partially implemented"
        }

        dataCapture -> crossCorrelate "captured voltage data flow into" {
            tags "Main flow"
        }
        dataCapture -> computeWeights "captured flag data flow into" {
            tags "Main flow"
        }
        dataCapture -> computeUVW "captured timestamps flow into"
        computeWeights -> normalizeVisibilities "correlation weights flow into" {
            tags "Main flow"
        }
        crossCorrelate -> normalizeVisibilities "correlated values flow into" {
            tags "Main flow"
        }
        normalizeVisibilities -> preCalFlagging "visibilities flow into" {
            tags "Main flow"
        }
        computeBaselines -> getAntPositions "gets positions from"
        computeUVW -> computeBaselines "gets baselines from"
        computeUVW -> preCalFlagging "uvw data (synchronized) flow into"
        preCalFlagging -> applyCal "visibilities flow into" {
            tags "Main flow"
        }
        preCalFlagging -> solveCal "visibilities occasionally flow into"
        applyCal -> solveCal "gets cal solutions from"
        applyCal -> postCalFlagging "visibilities flow into" {
            tags "Main flow"
        }
        solveCal -> exportCal "sends solutions to"
        postCalFlagging -> gridVisibilities "visibilities flow into" {
            tags "Main flow"
        }
        gridVisibilities -> exportGrids "grids occasionally flow into" {
            tags "Main flow"
        }
    }

    ipl2 = container "Secondary imaging pipeline" "Creates images from voltage data in a zoom band" "Legion" {
        getAntPositions = component "Get positions" "Get antenna positions" {
            tags "Unimplemented"
        }
        computeBaselines = component "Compute baselines" "Compute baselines" "Kokkos(CUDA)" {
            tags "Implemented"
        }
        dataCapture = component "Data capture" "Capture and sort voltage data packets" "UDP/Ethernet,libfabric" {
            tags "Implemented"
        }
        crossCorrelate = component "Cross correlation" "Correlate voltage data to produce visibilities" "TCC(CUDA)" {
            tags "Implemented"
        }
        computeWeights = component "Compute weights" "Compute visibility weights" "Kokkos(CUDA)" {
            tags "Implemented"
        }
        normalizeVisibilities = component "Normalize" "Normalize visibilities by weights" "Kokkos(CUDA)" {
            tags "Implemented"
        }
        computeUVW = component "Compute UVW" "Compute UVW coordinates" "Kokkos(CUDA)" {
            tags "Partially implemented"
        }
        preCalFlagging = component "Pre-calibration flagging" "Flag uncalibrated visibilities" {
            tags "Unimplemented"
        }
        applyCal = component "Apply cal solutions" "Apply calibration solutions to visibilities" {
            tags "Unimplemented"
        }
        postCalFlagging = component "Post-calibration flagging" "Flag calibrated visibilities" {
            tags "Unimplemented"
        }
        gridVisibilities = component "Gridding" "Grid visibilities" "Kokkos(CUDA)" {
            tags "Implemented"
        }
        exportGrids = component "Export grids" "Export image grids" {
            tags "Partially implemented"
        }

        dataCapture -> crossCorrelate "captured voltage data flow into" {
            tags "Main flow"
        }
        dataCapture -> computeWeights "captured flag data flow into" {
            tags "Main flow"
        }
        dataCapture -> computeUVW "captured timestamps flow into"
        computeWeights -> normalizeVisibilities "computed weights flow into" {
            tags "Main flow"
        }
        crossCorrelate -> normalizeVisibilities "correlated values flow into" {
            tags "Main flow"
        }
        normalizeVisibilities -> preCalFlagging "visibilities flow into" {
            tags "Main flow"
        }
        computeBaselines -> getAntPositions "gets positions from"
        computeUVW -> computeBaselines "gets baselines from"
        computeUVW -> preCalFlagging "uvw data (synchronized) flow into"
        preCalFlagging -> applyCal "visibilities flow into" {
            tags "Main flow"
        }
        applyCal -> postCalFlagging "visibilities flow into" {
            tags "Main flow"
        }
        postCalFlagging -> gridVisibilities "visibilities flow into" {
            tags "Main flow"
        }
        gridVisibilities -> exportGrids "grids occasionally flow into" {
            tags "Main flow"
        }
    }

    ipl2.applyCal -> ipl.exportCal "gets cal solutions from"
    ipl.exportGrids -> fs "writes images to"
    ipl2.exportGrids -> fs "writes images to"

    ftd = container "Fast-transient detection pipeline"
}

#
# deployment
#
!constant channelsPerStream 4
!constant streamsPerGpu 1

# IPL
!constant iplGpusPerNode 8
# IPL slice size is number of channels used for calibration
!constant iplChannelsPerSlice 32
# iplSlicesPerNode = (iplGpusPerNode * streamsPerGpu * channelsPerStream) / iplChannelsPerSlice
!constant iplSlicesPerNode 1

# IPL2
!constant ipl2GpusPerNode 10
# IPL2 slice size is number of channels processed per GPU
# ipl2ChannelsPerSlice = streamsPerGpu * channelsPerStream
!constant ipl2ChannelsPerSlice 4
# ipl2SlicesPerNode = (ipl2GpusPerNode * streamsPerGpu * channelsPerStream) / ipl2ChannelsPerSlice
!constant ipl2SlicesPerNode 10

!constant numNCl 5504
!constant numNCh 4496
!constant numAC 4192
!constant numBC 960

rcpDeployment = deploymentEnvironment "Production-RCP" {
    rcpLNodes = deploymentGroup "RCP-L nodes"
    rcpHNodes = deploymentGroup "RCP-H nodes"
    zoomNodes = deploymentGroup "Zoom band nodes"

    deploymentNode "RCP-L node" "Node with ${iplGpusPerNode} GPUs" {
        deploymentNode "RCP-L IPL instance" "RCP-L failure domain; a single Legion machine instance" {
            containerInstance rcp.ipl rcpLNodes
            instances "${iplSlicesPerNode}"
        }
        # instances = ceil(numNCl / (iplChannelsPerSlice * iplSlicesPerNode))
        instances 172
    }

    deploymentNode "RCP-H node" "Node with ${iplGpusPerNode} GPUs" {
        deploymentNode "RCP-H IPL instance" "RCP-H failure domain; a single Legion machine instance" {
            containerInstance rcp.ipl rcpHNodes
            instances "${iplSlicesPerNode}"
        }
        # instances = ceil(numNCh / (iplChannelsPerSlice * iplSlicesPerNode))
        instances 141
    }

    deploymentNode "Zoom band node" "Node with ${ipl2GpusPerNode} GPUs" {
        deploymentNode "RCP-A IPL instance" "RCP-A failure domain; a single Legion machine instance" {
            containerInstance rcp.ipl2 zoomNodes
            instances "0..${ipl2SlicesPerNode}"
        }
        deploymentNode "RCP-B IPL instance" "RCP-B failure domain; a single Legion machine instance" {
            containerInstance rcp.ipl2 zoomNodes
            instances "0..${ipl2SlicesPerNode}"
        }
        # instances = ceil((numAC + numBC) / (ipl2ChannelsPerSlice * ipl2SlicesPerNode))
        instances 129
    }
}
