#
# Data management sub-system (WBS L2: DAT)
#
dat = softwareSystem "Data management" {
    sharedFilesystem = container "Shared file system" "Data center network file system" "Lustre" {
        tags "filesystem"
    }
    database = container "Data Management Database System" "database to manage state and location of data products" "" {
        tags "database"
    }
    taskScheduler = container "Task Scheduler" "Software to manage processing of tasks" "" {
    }

    commonInitialImageProc = container "Common Initial Image Processing" "Task for processing images before other tasks" "" {
    }
    contImageProc = container "Continuum Image Processing" "Task for processing broadband continuum images" "" {
    }
    polImageProc = container "Polarimetric Image Processing" "Task for processing polarimetric images" "" {
    }
    lowresHIImageProc = container "Low-resolution HI Image Processing" "Task for processing low-res HI images" "" {
    }
    zoomaImageProc = container "Zoom A Image Processing" "Task for processing Zoom A images" "" {
    }
    zoombImageProc = container "Zoom B Image Processing" "Task for processing Zoom B images" "" {
    }
    commonFinalImageProc = container "Common Final Image Processing" "Task for processing images after other imaging tasks" "" {
    }
    fastpertProc = container "Fast Periodic Timing Processing" "Task for processing Fast Periodic Timing data" "" {
    }
    fastpersProc = container "Fast Periodic Search Processing" "Task for processing Fast Periodic Search data" "" {
    }
    fasttranProc = container "Fast Transient Processing" "Task for processing Fast Transient data" "" {
    }
    archiveprepProc = container "Public Archive Preparation Processing" "Task for processing all data for moving to public archive" "" {
    }

    taskScheduler -> contImageProc "manages"
    contImageProc -> sharedFilesystem "writes to"
    contImageProc -> taskScheduler "notifies"
    taskScheduler -> polImageProc "manages"
    polImageProc -> sharedFilesystem "writes to"
    taskScheduler -> lowresHIImageProc "manages"
    lowresHIImageProc -> sharedFilesystem "writes to"
    taskScheduler -> zoomaImageProc "manages"
    zoomaImageProc -> sharedFilesystem "writes to"
    taskScheduler -> zoombImageProc "manages"
    zoombImageProc -> sharedFilesystem "writes to"

    taskScheduler -> commonInitialImageProc "manages"
    commonInitialImageProc -> sharedFilesystem "writes to"
    commonFinalImageProc -> sharedFilesystem "writes to"
    #commonFinalImageProc -> database "stores processing state in"
    taskScheduler -> commonFinalImageProc "manages"

    taskScheduler -> fastpertProc "manages"
    taskScheduler -> fastpersProc "manages"
    taskScheduler -> fasttranProc "manages"

    taskScheduler -> archiveprepProc "manages"
    #archiveprepProc -> database "stores processing state in"
    taskScheduler -> database "updates"
}
