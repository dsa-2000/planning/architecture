#
# DSA-2000 software architecture model definition
#
model {
    # DSA-2000 systems
    group "DSA-2000" {
        # Sub-system definitions, at WBS L2. Each such sub-system may
        # comprise one or more softwareSystems, including constituent
        # elements (containers and components, along with any purely
        # internal relationships), but not relationships that cross
        # sub-system boundaries.
        !include dat.dsl
        !include mc.dsl
        !include opl.dsl
        !include rcf.dsl
        !include rcp.dsl
        !include arc.dsl
        !include pt.dsl
        !include ast.dsl

        # System definitions, primarily people (users, roles, etc),
        # and relationships that cross (L2) sub-system boundaries or
        # involve people.
        !include dsa2k.dsl

        # Software engineering support is a softwareSystem that
        # doesn't (yet) fit into any WBS L2 sub-system.
        swEngineering = softwareSystem "Software engineering support" "For software build, test and deployment" {
            gitlabRunner = container "gitlab runner" "Executes continuous integration and deployment tasks"
        }
        localRunnerDeployment = deploymentEnvironment "Local runners" {
            deploymentNode "Local environment" "Lab or test array environment" {
                deploymentNode "Runner instance" "Node with gitlab runner" {
                    containerInstance swEngineering.gitlabRunner
                    instances "1..*"
                }
                instances "0..*"
            }
        }
    }
    # External system dependencies
    group "gitlab.com" {
        gitlabService = softwareSystem "CI/CD service" "Continuous build, test, integration and deployment service"
    }
    group "aws.amazon.com" {
        s3 = softwareSystem "Cloud storage" "Software artifacts storage"
    }
    group "NANOGrav" {
        nanograv = softwareSystem "NANOGrav" "NANOGrav consortium"
    }

    #
    # people
    #
    operator = person "Array operator" "A person controlling array functionality internally. May have any of a number of roles."
    developer = person "Software developer" "A person that writes or tests software"
    engineer = person "Engineer" "A person conducting array engineering tasks"
    scientist = person "Scientific user" "A person that access final data products"

    # Relationships of DSA-2000 elements with external system elements
    developer -> gitlabService "pushes code commits, merges pull requests, maintains CI/CD pipelines using"
    operator -> gitlabService "manages code deployments using"
    gitlabService -> swEngineering.gitlabRunner "schedules build, test and deployment tasks on"
    gitlabService -> s3 "accesses software artifacts on"
    nanograv -> arc "retrieves folded Stokes profiles from"

    scientist -> arc "accesses data products in"
    operator -> opl "uses"
    engineer -> opl "uses"
    operator -> mc.control "uses"
    engineer -> mc.control "uses"
}
