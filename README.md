# Architecture documentation

## Presentation view

If you wish simply to view the software architecture documentation,
please follow [this
link](https://structurizr.com/share/82100/documentation "DSA-2000
software architecture") for a nicely rendered, interactive view.

If you prefer a more diagram-centric view, please follow [this
link](https://structurizr.com/share/82100/diagrams "diagrams-only
architecture view") where you may find an entry point with an improved
UX for interacting with the diagrams alone.

## For developers

This repository is for the development and maintenance of the DSA-2000
software architecture. The architecture is documented using
the [Structurizr DSL](https://github.com/structurizr/dsl "Structurizr
DSL repo") (domain specific language), which is based on the [C4
model](https://c4model.com "C4 model web site") for the description of
software architectures. The plain text DSL files are supplemented with
Markdown files to create a traditional document-oriented description
with embedded diagrams. As both types of these files are in plain
text, collaborative development of the software architecture, mediated
through git, is well supported.

On gitlab.com, a CI pipeline pushes updates to this repository to the
structurizr.com
[DSA-2000](https://structurizr.com/workspace/82100/documentation
"architecture workspace") web site, which produces the presentation view
mentioned above. Also see [this
link](https://structurizr.com/workspace/82100/diagrams "diagrams-only
architecture workspace") for the diagram-centric view. Note that these last
two links provide access to the project "workspace" on
structurizr.com, which may provide you with additional capabilities on
that web site if you are a project member there with the appropriate
role; however, this repository and CI pipeline have been designed
primarily to support development of the architecture documents through
the gitlab.com repository, not structurizr.com.

If you're an emacs user, you might want to check out
[szr-mode](https://gitlab.com/dsa-2000/code/common/szr-mode) for a
Structurizr DSL file mode definition.
